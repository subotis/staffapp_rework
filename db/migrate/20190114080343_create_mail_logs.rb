class CreateMailLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :mail_logs do |t|
      t.belongs_to :post, index: true
      t.integer :user_notified
      t.integer :user_readed

      t.timestamps
    end
  end
end
