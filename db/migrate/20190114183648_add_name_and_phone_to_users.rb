class AddNameAndPhoneToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :name, :string
    add_column :users, :phone, :string, :limit => 30
  end
end
