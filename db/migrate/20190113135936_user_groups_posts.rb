class UserGroupsPosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts_user_groups, id: false do |t|
      t.belongs_to :post, index: true
      t.belongs_to :user_group, index: true
    end
  end
end
