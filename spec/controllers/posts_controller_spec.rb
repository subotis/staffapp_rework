require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  describe "GET index" do
    it "show a list of all posts" do
      get :index
      expect(response).to be_success
    end
  end
end
