require 'rails_helper'

RSpec.describe Post, type: :model do
  it "creates a valid model" do
    post = Post.new(
        title: 'Test title',
        text: 'Lorem ipsum'
    )
    post.image.attach(io: File.open("#{Rails.root}" + "/app/assets/images/home-bg.jpg"), filename: "face.jpg", content_type: "image/jpg")
    expect(post).to be_valid
  end

  it "is invalid without a title" do
    post = Post.new(title: nil)
    post.valid?
    expect(post.errors[:title]).to include("can't be blank")
  end
  it "is invalid with long title" do
    post = Post.new(title: 'a'*500)
    post.valid?
    expect(post.errors[:title]).to include("pick a shorter title")
  end
  it "is invalid with short title" do
    post = Post.new(title: 'a'*5)
    post.valid?
    expect(post.errors[:title]).to include("pick a longer title")
  end
  it "is invalid without text" do
    post = Post.new(text: nil)
    post.valid?
    expect(post.errors[:text]).to include("can't be blank")
  end

  it "sends an email" do
    post = Post.new(
        title: 'Test title',
        text: 'Lorem ipsum'
    )
    post.image.attach(io: File.open("#{Rails.root}" + "/app/assets/images/home-bg.jpg"), filename: "face.jpg", content_type: "image/jpg")
   # FooBar.should_receive(:create).with(eventual_value { { id: foo1.id } })

    expect(PostMailer).to receive(:new_post).with(:post, :user).exactly(User.all.size).times
    #expect post to change { ActionMailer::Base.deliveries.count }.by(1)
  end
end
