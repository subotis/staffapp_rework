class MailLog < ApplicationRecord
  belongs_to :post
  has_many :notified_users
  has_many :users, through: :notified_users

end
