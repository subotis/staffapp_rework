class Post < ApplicationRecord
  has_and_belongs_to_many :user_groups, optional: true
  has_one :mail_log
  has_one_attached :image
  has_many_attached :files
  after_create :send_notify
  validates_presence_of :title
  validates_length_of :title, :within => 6..50, :too_long => "pick a shorter title", :too_short => "pick a longer title"
  validates_presence_of :text

  private

  def send_notify
    PostLoggerService.send_notify(self)
  end
end
