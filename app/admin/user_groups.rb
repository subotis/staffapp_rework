ActiveAdmin.register UserGroup do
  permit_params :name, user_ids: [], post_ids: []
  config.sort_order = 'id_asc'
  remove_filter :created_at, :updated_at
  index do
    selectable_column
    column 'Name' do |n|
      link_to(n.name, admin_user_group_path(n))
    end
    column 'Users' do |u|
      safe_join(u.users.map{|user| link_to(user.name, admin_user_path(user))},', '.html_safe)
    end
    column 'Posts' do |p|
      safe_join(p.posts.map{|u| link_to(u.title, admin_post_path(u))}, ", ".html_safe)
    end
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row "Users" do
        user_group.users.size
      end
      panel "Users" do
        table_for user_group.users do
          column "name" do |ug|
            ug.name
          end
        end
      end
      row "Posts" do
        user_group.posts.size
      end
      panel "Posts" do
        table_for user_group.posts do
          column "Title" do |ug|
            ug.title
          end
        end
      end
    end
  end

  form do |f|
    f.inputs "User Group Details" do
      f.input :name
      f.input :user_ids, as: :check_boxes, collection: User.all
      f.input :post_ids, as: :check_boxes, collection: Post.all
      f.button :Submit
    end
  end
  controller do
    def scoped_collection
      super.includes(:posts).includes(:users)
    end
  end
end
