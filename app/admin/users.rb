ActiveAdmin.register User do
  permit_params :email, :is_admin, :password, :password_confirmation, user_group_ids: []
  config.sort_order = 'id_asc'
  remove_filter :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at, :created_at, :updated_at
  index do
    selectable_column
    column 'Name' do |n|
      link_to(n.name, admin_user_path(n))
    end
    column :email
    column :is_admin
    column 'User Groups' do |ug|
      safe_join(ug.user_groups.map{|u| link_to(u.name, admin_user_group_path(u))}, ", ".html_safe)
    end
    column :phone
    actions
  end

  show do
    attributes_table do
      row :name
      row :email
      row :is_admin
      row :phone
      panel "User Groups" do
        table_for user.user_groups do
          column "name" do |ug|
            ug.name
          end
          column "users" do |u|
            u.users.size
          end
          column "posts" do |u|
            u.posts.size
          end
        end
      end
    end
  end

  form do |f|
    f.inputs "User Details" do
      f.input :name
      f.input :phone
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :is_admin
      f.input :user_group_ids, as: :check_boxes, collection: UserGroup.all
    f.button :Submit
      end
  end

  controller do
    def update
      if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
      super
    end
    def scoped_collection
      super.includes(:user_groups)
    end
  end
end
