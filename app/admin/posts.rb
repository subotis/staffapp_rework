ActiveAdmin.register Post do
  permit_params :title, :text, :image, files: [], user_group_ids: []
  config.sort_order = 'id_asc'

  remove_filter :created_at, :updated_at, :mail_log, :image_blob, :files_blobs, :image_attachment, :files_attachments
  index do
    selectable_column
    column 'Title' do |t|
      link_to(t.title, admin_post_path(t))
    end
    column "Link sended" do |l|
      l.mail_log.user_notified if l.mail_log
    end
    column "Users readed post" do |l|
      l.mail_log.user_readed if l.mail_log
    end
    column 'User Groups' do |u|
      safe_join(u.user_groups.map{|u| link_to(u.name, admin_user_group_path(u))}, ", ".html_safe)
    end
    column "Percent" do |p|
      div class: 'progress' do
        if p.mail_log
          div class: 'progress-bar bg-success', role: 'progressbar', style:"width: #{p.mail_log.user_readed*100/p.mail_log.user_notified}%;" do
            "#{p.mail_log.user_readed*100/p.mail_log.user_notified}%"
          end
        end
      end
    end
    column "Date" do |d|
      d.created_at.to_formatted_s(:short)
    end
    actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :text
      row :image do |i|
        image_tag url_for(i.image.variant(resize: "300x300")) if i.image.attached?
      end
      panel "User Groups" do
        table_for post.user_groups do
          column "name" do |ug|
            ug.name
          end
          column "users" do |u|
            u.users.size
          end
          column "posts" do |u|
            u.posts.size
          end
        end
      end
      panel "Attachments" do
        table_for post.files do
          column "name" do |ug|
            link_to ug.filename, rails_blob_path(ug, disposition: "attachment")
          end
        end
      end
    end
  end

  form html: { multipart: true } do |f|
    f.inputs "Post Details" do
      f.input :image, as: :file
      f.input :title
      f.input :text, as: :froala_editor, input_html: {data: {options: {toolbarButtons: ['undo', 'redo', '|', 'bold', 'italic']}}}
      f.input :user_group_ids, as: :check_boxes, collection: UserGroup.all, :label => 'User Groups'
      f.input :files, as: :file, input_html: { multiple: true }
      f.button :Submit
    end
  end

  controller do
    before_action :reload_scopes, :only => :index

    def reload_scopes
      resource = active_admin_config
      UserGroup.pluck(:id, :name).each do |id, name|
        next if resource.scopes.any? { |scope| scope.name == name }
        resource.scopes << (ActiveAdmin::Scope.new name do
         Post.joins(:user_groups).where(:posts_user_groups => {user_group_id: id})
          end)
      end
      # remove scope if usergroup was deleted
      resource.scopes.delete_if do |scope|
        !(UserGroup.pluck(:name).any? { |m| scope.name == m } || ['all'].include?(scope.name)) # don't delete other scopes you have defined
      end
    end

    def scoped_collection
      super.includes(:user_groups).includes(:mail_log)
    end
  end
end
