module PostsHelper
  def show_attachments?(post)
    return current_user.user_group_ids.any? { |i| post.user_group_ids.include? i} if user_signed_in?
    return false
  end
end
