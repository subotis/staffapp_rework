class PostMailer < ApplicationMailer
  default from: 'notifications@example.com'

  def new_post
    @post = params[:post]
    @user = params[:user]
    mail(to: @user.email, subject: 'Now post is created')
  end
end
