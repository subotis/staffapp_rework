class PostsController < ApplicationController
  before_action :set_post, :set_user, only: [:show]
  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.with_attached_image
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    PostLoggerService.update_users(@post,@user )
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.with_attached_image.with_attached_files.find(params[:id])
    end
    def set_user
      @user = User.find_by_email(Base64.urlsafe_decode64(params[:user])) if params[:user]
      sign_in(@user) if @user
      @user = current_user
    end
end
