class ApplicationController < ActionController::Base
  add_flash_types :success, :warning, :danger, :info
  def authenticate_user!
    redirect_to root_path, info:'You are not allowed there' unless current_user.try(:is_admin?)
  end
end
