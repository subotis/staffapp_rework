module PostLoggerService
  class << self
    def send_notify(post)
      MailLog.create(post_id: post.id, user_notified: User.all.size, user_readed: 0)
      User.find_each do |user|
        PostMailer.with(post: post, user: user).new_post.deliver_later
      end
    end

    def update_users(post, user)
      log = MailLog.find_by_post_id(post.id)
      if log && user
        unless log.users.include?(user)
          log.update_column 'user_readed', log.user_readed + 1
          log.users << user
        end
      end
    end
  end
end