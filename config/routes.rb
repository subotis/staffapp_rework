Rails.application.routes.draw do
  devise_for :users, ActiveAdmin::Devise.config
  resources :posts, only: [:show] do
    member do
      get ':user', to: 'posts#show'
    end
  end

  ActiveAdmin.routes(self)
  root 'posts#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
